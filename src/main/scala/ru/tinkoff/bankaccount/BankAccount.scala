package ru.tinkoff.bankaccount

import java.time.LocalDate
import java.util.concurrent.atomic.AtomicLong

import enumeratum.{Enum, EnumEntry}
import ru.tinkoff.bankaccount.AccountType.{Credit, Debit, Deposit, Saving}

case class BankAccount[T <: AccountType](accountId: Long,
                                         balance: BigDecimal,
                                         openDate: LocalDate,
                                         closeDate: Option[LocalDate],
                                         accountType: AccountType,
                                         tariff: Tariff)

sealed trait Tariff
object Tariff {
  case object Default extends Tariff
}

sealed trait AccountType extends EnumEntry
object AccountType extends Enum[AccountType] {
  case object Debit extends AccountType
  case object Saving extends AccountType
  case object Credit extends AccountType
  case object Deposit extends AccountType

  override val values: IndexedSeq[AccountType] = findValues
}

object AccountCreatorService {
  val ACCOUNT_SEQ: AtomicLong = new AtomicLong()

  @throws(classOf[IllegalArgumentException])
  def openAccount[T <: AccountType](accountType: T, balance: BigDecimal = 0, closeDate: Option[LocalDate] = None): BankAccount[T] = {
    if (accountType.isInstanceOf[Credit.type ] && balance > 0) {
      throw new IllegalArgumentException("Balance can't be positive for credit account type.");
    }
    if (!accountType.isInstanceOf[Credit.type] && balance < 0) {
      throw new IllegalArgumentException("Balance should be negative for credit account type only.")
    }

    BankAccount(ACCOUNT_SEQ.incrementAndGet(), balance, LocalDate.now(), closeDate, accountType, Tariff.Default)
  }
}

object AccountProlongationService {
  def prolongation(bankAccount: BankAccount[_], newCloseDate: LocalDate): BankAccount[_] = {
    bankAccount.copy(closeDate = Some(newCloseDate))
  }
}

object AccountTransferService {

  implicit class TransferFromDebit[T <: Debit.type](from: BankAccount[T]) {
    def transferTo(to: BankAccount[_], value: BigDecimal): (BankAccount[_], BankAccount[_]) = {
      transfer(from, to, value)
    }
  }

  implicit class TransferFromSaving[T <: Saving.type](from: BankAccount[T]) {
    def transferTo(to: BankAccount[_], value: BigDecimal): (BankAccount[_], BankAccount[_]) = {
      transfer(from, to, value)
    }
  }

  private def transfer(from: BankAccount[_], to: BankAccount[_], value: BigDecimal): (BankAccount[_], BankAccount[_]) = {
    val pureValue = to.accountType match {
      case AccountType.Credit => to.balance.abs.min(value)
      case _ => from.balance.min(value)
    }
    val newFrom = from.copy(balance = from.balance - pureValue)
    val newTo = to.copy(balance = to.balance + pureValue)
    (newFrom, newTo)
  }
}

object AccountCloseService {

  implicit class CloseSaving[T <: Saving.type](bankAccount: BankAccount[T]) {
    def closeAccount(): BankAccount[T] = {
      bankAccount.copy(closeDate = Some(LocalDate.now()))
    }
  }

  implicit class CloseDeposit[T <: Deposit.type](bankAccount: BankAccount[T]) {
    def closeAccount(): BankAccount[T] = {
      bankAccount.copy(closeDate = Some(LocalDate.now()))
    }
  }

}

object AccountBalanceService {
  def totalBalance(accounts: Iterable[BankAccount[_]]): BigDecimal = {
    accounts.map(_.balance).map(_.abs).sum
  }
}