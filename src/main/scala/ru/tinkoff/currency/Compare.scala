package ru.tinkoff.currency

import ru.tinkoff.currency.CompareResult.{Equals, Greater, Less}
import ru.tinkoff.currency.Currency.Rub

trait Comparator[T] {
  def compareWith(left: T, right: T): CompareResult
}

object Comparator {
  implicit class CurrencyNameCompareSyntax(left: Currency) {
    def compareWith(right: Currency)(implicit compare: Comparator[String]): CompareResult = {
      compare.compareWith(left.toString, right.toString)
    }
  }

  implicit class CurrencyRateCompareSyntax[O1 <: Currency, O2 <: Currency](left: Currency) {
    def compareWith(right: Currency)(implicit co: Comparator[BigDecimal],
                                     leftRate: Rate[O1, Rub.type],
                                     rightRate: Rate[O2, Rub.type]): CompareResult = {
      co.compareWith(leftRate.rate(), rightRate.rate())
    }
  }
}

object ComparatorUtils {
  implicit val compareCurrencyByName: Comparator[String] = (left: String, right: String) => {
    val x = left.compareTo(right)
    if (x < 0) Less
    else if (x == 0) Equals
    else Greater
  }

  implicit val compareCurrencyByRateInRub: Comparator[BigDecimal] = (left: BigDecimal, right: BigDecimal) => {
    val x = left.compareTo(right)
    if (x < 0) Less
    else if (x == 0) Equals
    else Greater
  }
}

sealed trait CompareResult
object CompareResult {
  case object Less extends CompareResult
  case object Greater extends CompareResult
  case object Equals extends CompareResult
}

trait Rate[S, T] {
  def rate(): BigDecimal
}

object Rate {
  implicit val usdToRub: Rate[Currency.Usd.type, Currency.Rub.type] = () => 75
  implicit val rubToUsd: Rate[Currency.Rub.type, Currency.Usd.type] = () => BigDecimal(1) / 75
  implicit val eurToRub: Rate[Currency.Eur.type, Currency.Rub.type] = () => 87
  implicit val rubToEur: Rate[Currency.Rub.type, Currency.Eur.type] = () => BigDecimal(1) / 87
  implicit val rubToRub: Rate[Currency.Rub.type, Currency.Rub.type] = () => 1
}