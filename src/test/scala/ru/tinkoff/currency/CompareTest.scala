package ru.tinkoff.currency

import org.scalatest.{FlatSpec, Matchers}
import ru.tinkoff.currency.Currency.{Eur, Rub, Usd}

class CompareTest extends FlatSpec with Matchers {
  it should "comparator test" in {
    import Comparator.CurrencyNameCompareSyntax
    import ComparatorUtils.compareCurrencyByName

    (Usd.compareWith(Rub) == CompareResult.Greater) shouldBe true
    (Rub.compareWith(Usd) == CompareResult.Less) shouldBe true
    (Eur.compareWith(Rub) == CompareResult.Less) shouldBe true
    (Rub.compareWith(Eur) == CompareResult.Greater) shouldBe true
    (Eur.compareWith(Usd) == CompareResult.Less) shouldBe true
    (Usd.compareWith(Eur) == CompareResult.Greater) shouldBe true
  }
}
