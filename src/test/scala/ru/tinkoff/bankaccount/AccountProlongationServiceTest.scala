package ru.tinkoff.bankaccount

import java.time.LocalDate

import org.scalatest.{FlatSpec, Matchers}

class AccountProlongationServiceTest extends FlatSpec with Matchers {
  it should "close date should be refreshed after set" in {
    val newCloseDate = LocalDate.now()
    val bankAccount = AccountCreatorService.openAccount(AccountType.Deposit)
    bankAccount.closeDate.isDefined shouldBe false
    AccountProlongationService.prolongation(bankAccount, newCloseDate).closeDate.get shouldBe newCloseDate
  }
}
