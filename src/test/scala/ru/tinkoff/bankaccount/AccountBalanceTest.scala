package ru.tinkoff.bankaccount

import org.scalatest.{FlatSpec, Matchers}

class AccountBalanceTest extends FlatSpec with Matchers {
  it should "calculate account balance" in {
    val profile = new Array[BankAccount[_]](4)
    profile(0) = AccountCreatorService.openAccount(AccountType.Debit, 100)
    profile(1) = AccountCreatorService.openAccount(AccountType.Credit, -200)
    profile(2) = AccountCreatorService.openAccount(AccountType.Saving, 300)
    profile(3) = AccountCreatorService.openAccount(AccountType.Deposit, 600)

    val balance: BigDecimal = AccountBalanceService.totalBalance(profile)
    balance shouldBe 1200
  }
}
