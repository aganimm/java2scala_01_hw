package ru.tinkoff.bankaccount

import org.scalatest.{FlatSpec, Matchers}

class AccountCreatorServiceTest extends FlatSpec with Matchers {
  it should "can't create credit with positive balance" in {
    var isCatch = false
    try {
      AccountCreatorService.openAccount(AccountType.Credit, 10)
    } catch {
      case _: IllegalArgumentException => isCatch = true
    }
    isCatch shouldBe true
  }

  it should "can't create debit with negative balance" in {
    var isCatch = false
    try {
      AccountCreatorService.openAccount(AccountType.Debit, -10)
    } catch {
      case _: IllegalArgumentException => isCatch = true
    }
    isCatch shouldBe true
  }

  it should "should create debit with zero balance" in {
    var isCatch = false
    try {
      AccountCreatorService.openAccount(AccountType.Debit)
    } catch {
      case _: IllegalArgumentException => isCatch = true
    }
    isCatch shouldBe false
  }
}