package ru.tinkoff.bankaccount

import org.scalatest.{FlatSpec, Matchers}

class AccountCloseServiceTest extends FlatSpec with Matchers {
  it should "close date should be after close deposit account type" in {
    import AccountCloseService.CloseDeposit

    val ba = AccountCreatorService.openAccount(AccountType.Deposit)
    ba.closeDate.isDefined shouldBe false
    ba.closeAccount().closeDate.isDefined shouldBe true
  }

  it should "close date should be after close saving account type" in {
    import AccountCloseService.CloseSaving

    val ba = AccountCreatorService.openAccount(AccountType.Saving)
    ba.closeDate.isDefined shouldBe false
    ba.closeAccount().closeDate.isDefined shouldBe true
  }
}
