package ru.tinkoff.bankaccount

import org.scalatest.{FlatSpec, Matchers}
import ru.tinkoff.bankaccount.AccountType.{Credit, Debit, Saving}
import scala.language.existentials

class AccountTransferServiceTest extends FlatSpec with Matchers {
  it should "check balance after transfer from debit" in {
    import AccountTransferService.TransferFromDebit

    val from = AccountCreatorService.openAccount(Debit, 1000)
    val to = AccountCreatorService.openAccount(Debit, 300)

    val result = from.transferTo(to, 800)
    result._1.balance shouldBe 200
    result._2.balance shouldBe 1100
  }

  it should "check balance after transfer from saving" in {
    import AccountTransferService.TransferFromSaving

    val from = AccountCreatorService.openAccount(Saving, 1000)
    val to = AccountCreatorService.openAccount(Credit, -600)

    val result = from.transferTo(to, 700)
    result._1.balance shouldBe 400
    result._2.balance shouldBe 0
  }
}
